//allows for a timer
class Timer {
  
  int startTime;
  int endTime;
  boolean isRunning;
  int timePassed;
  int pauseStart;
  int pauseEnd;
  int pauseTime;
  

  Timer(int seconds){
    setTotalTime(seconds);
    isRunning = false;
  }
  
  //start timer at 0
  void start(){
    startTime = millis();
    pauseStart = startTime;
    pauseEnd = startTime;
    isRunning =  true;
  }

  //allow a endtime to be set
  void setTotalTime(int seconds){
    endTime = seconds * 1000;
    isRunning = false;
  }
  
  //checks if timer has ended
  boolean hasEnded(){
    timePassed = millis() - startTime - (pauseTime);
    if (timePassed >= endTime && isRunning){
      isRunning = false;
      return true;
    }
    else{
      return false;
    }
  }
 
  //allows timer to be paused and unpaused
  void pause(){
    if (isRunning) {
      pauseStart = millis();
      timePassed = millis() - startTime;
      isRunning = false;
    }
  }
  void unpause(){
    if (!isRunning) {
    pauseEnd = millis();
    isRunning = true;
    pauseTime += (pauseEnd - pauseStart);
    timePassed = millis() - startTime - (pauseTime);
    }
  }
  
  int getTimePassed(){ 
    timePassed = millis() - startTime - (pauseTime);
    return timePassed;
  }
  
}
