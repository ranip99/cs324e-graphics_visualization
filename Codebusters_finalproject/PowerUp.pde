class PowerUp {
  Spaceship player;
  float y = 0;
  float x;
  PShape lightning;
  boolean has_poweredup=false;
  int powerupstart;
  
  PowerUp(Spaceship player, float x){
    this.x =x;
    this.player=player;
    fill(238,242,34);
    lightning=createShape();
    lightning.beginShape();
    lightning.vertex(20,-20);
    lightning.vertex(10,-15);
    lightning.vertex(25,-55);
    lightning.vertex(5,-50);
    lightning.vertex(-10,0);
    lightning.vertex(0,-5);
    lightning.vertex(-10,40);
    lightning.endShape(CLOSE);
  }
  
  void display(){
    fill(255);
    
    if (has_poweredup==false){
    if((player.y-y)<=20){
       if ((player.y-y)<=0 && abs(player.x-x)<=25 && (y-player.y)<=95){
        has_poweredup=true;
        //get current time
        powerupstart=countdown.getTimePassed();

      }
      //near tip harder to collide
      else if ((player.y-y)<=20 && abs(player.x-x)<=10 && (y-player.y)<=95){
        has_poweredup=true;
        powerupstart=countdown.getTimePassed();

      }
     }
    }

    //if you haven't collided, still show the powerup object
    if (has_poweredup==false){
    pushMatrix();
    translate(x,y);
    scale(0.5);
    shape(lightning, 0, 0);
    y++;
    popMatrix();
    player.powerupstate=false;
      }
    else if (has_poweredup==true){
      //after powered up, only 20 seconds of power up state
      if ((countdown.getTimePassed()-powerupstart)<=20000){
      player.powerupstate=true;
      }
      else{
        player.powerupstate=false;
      }
    }
    else {
      player.powerupstate=false;
    }
     
    
    }
   
   void restart(){
     has_poweredup=false;
     player.powerupstate=false;
     y=0;
   }
  }
