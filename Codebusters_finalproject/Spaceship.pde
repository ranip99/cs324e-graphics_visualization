class Spaceship {
  int score;
  int player;
  float y = height-100.0;
  float initial_x, initial_y;
  float x;
  PShape spaceship;
  boolean goUp, goDown, goRight, goLeft;
  boolean powerupstate=false;
  float speed=2.5;
  PShape flame;
  PShape star;
  int spin=0;
  
  Spaceship(int player, float x){
    this.score = 0;
    this.player = player;
    this.x =x;
    this.initial_x = x;
    this.initial_y = y;
    this.goUp = false;
    this.goDown = false;
    this.goRight = false;
    this.goLeft = false;
    
    //load in flames and star for powerup 
    flame=loadShape("flame.svg");
    star=loadShape("star.svg");
    
    //each player has a different spaceship design
    if (player==1){
      //drawn from tip of spaceship (origin is at tip)
      spaceship = createShape(GROUP);
      fill(255,0,0);
      PShape tip = createShape(TRIANGLE, 0,0,-25, 30, 25, 30);
      fill(210,216,219);
      PShape body = createShape(RECT, -25,30,50,80);
      fill(133,153, 162);
      PShape windowframe = createShape(ELLIPSE, 0, 60, 40, 40);
      fill(136,211, 245);
      PShape window = createShape(ELLIPSE, 0, 60, 30, 30);
      fill(255,0,0);
      PShape fin1 = createShape(QUAD, -25, 85, -25, 110, -35, 150, -40, 90);
      PShape fin2 = createShape(QUAD, 25, 85, 25, 110, 35, 150, 40, 90);
      PShape fin3 = createShape(QUAD, 0, 85, 5, 90, 0, 150, -5, 90);
      spaceship.addChild(tip);
      spaceship.addChild(body);
      spaceship.addChild(windowframe);
      spaceship.addChild(window);
      spaceship.addChild(fin1);
      spaceship.addChild(fin2);
      spaceship.addChild(fin3);
    }
    else if (player==2){
      //drawn from tip of spaceship (origin is at tip)
      spaceship = createShape(GROUP);
      fill(46,82,206);
      PShape tip = createShape(TRIANGLE, 0,0,-25, 30, 25, 30);
      fill(210,216,219);
      PShape body = createShape(RECT, -25,30,50,80);
      fill(133,153, 162);
      PShape windowframe = createShape(ELLIPSE, 0, 50, 35, 35);
      fill(136,211, 245);
      PShape window = createShape(ELLIPSE, 0, 50, 25, 25);
      fill(133,153, 162);
      PShape windowframe2 = createShape(ELLIPSE, 0, 90, 35, 35);
      fill(136,211, 245);
      PShape window2 = createShape(ELLIPSE, 0, 90, 25, 25);
      fill(46,82,206);
      PShape fin1 = createShape(QUAD, -25, 85, -25, 110, -35, 150, -40, 90);
      PShape fin2 = createShape(QUAD, 25, 85, 25, 110, 35, 150, 40, 90);
      spaceship.addChild(tip);
      spaceship.addChild(body);
      spaceship.addChild(windowframe);
      spaceship.addChild(window);
      spaceship.addChild(windowframe2);
      spaceship.addChild(window2);
      spaceship.addChild(fin1);
      spaceship.addChild(fin2);
    }
    
  }
  
  void up(){
    y -=speed;
  }
  
  void down(){
    y+=speed;
  }
  
  void left(){
    x-=speed;
  }
  
  void right(){
    x+=speed;
  }

  
  void display(Boolean collision){
    
    if (powerupstate==true){
      speed=5.0;
      pushMatrix();
      //flame
      pushMatrix();
      flame.disableStyle();
      fill(255,129,3);
      shapeMode(CENTER);
      //stroke(255,255,0);
      translate(x,y+75);
      rotate(PI);
      scale(0.05);
      
      shape(flame, 0,0);
      popMatrix();
      

      //spinning stars
      star.disableStyle();
      fill(255,255,0);
      //star 1
      pushMatrix();
      translate(x+15,y);
      scale(0.02);
      rotate(spin/5);
      shape(star,0,0);
      popMatrix();
      //star 2
      pushMatrix();
      translate(x+25,y+20);
      scale(0.02);
      rotate(spin/5);
      shape(star,0,0);
      popMatrix();
      //star 3
      pushMatrix();
      translate(x-15,y);
      scale(0.02);
      rotate(spin/5);
      shape(star,0,0);
      popMatrix();
      //star 4
      pushMatrix();
      translate(x-25,y+20);
      scale(0.02);
      rotate(spin/5);
      shape(star,0,0);
      popMatrix();
      //star 5
      pushMatrix();
      translate(x,y-10);
      scale(0.02);
      rotate(spin/5);
      shape(star,0,0);
      popMatrix();
      spin++;
      shapeMode(CORNER);
      popMatrix();
      
    }
    else {
      //after powerupstate finished, return to normal state 
      speed=2.5;
    }
    
    
    //going up and haven't reached the top 
    if (goUp && y > 0){
      up();
    }
    //going down and haven't reached the bottom
    if (goDown && y < height){
     down();
    }
    
    if (goLeft &&  x>0){
      left();
    }
    
    if (goRight && x < width){
      right();
    }
    
    //reached the top 
    if (y<=0){
      x = initial_x;
      y = initial_y;
      score+=5;
    }
    
    //collision
    if (collision){
      x = initial_x;
      y = initial_y;
    }
    
    //goes beneath screen view
    if (y > height - 50){
      x = initial_x;
      y = initial_y;
    }
    
    pushMatrix();
    translate(x,y);
    scale(0.5);
    shape(spaceship, 0, 0 );
    popMatrix();
  }
  
  //determine score
  int Score(Boolean collision){
    //add 5 if reached top
    if (this.y <=1){
      x = initial_x;
      y = initial_y;
      score += 5;
      
    }
    //-1 if collided
    if (collision){
      x = initial_x;
      y = initial_y;
      score -= 1;
    }
    return score;
  }
 
}
