class Missile {
  float x, y, spacex, spacey;
  boolean off;
  
  Missile(float sx,float sy){
    spacex = sx;
    spacey = sy;
    off = false;
    this.x = spacex;
    this.y = spacey;
  }
  
  //display the missile
  void display() {
    //pushMatrix();
    //translate(spacex, spacey);
    color r = color (255,0,0);
    fill(r);
    triangle(this.x-10, this.y+10, this.x, this.y, this.x+10, this.y+10);
    triangle(this.x-20,this.y+50,this.x-7.5,this.y+40,this.x-7.5,this.y+50);
    triangle(this.x+20,this.y+50,this.x+7.5,this.y+40,this.x+7.5,this.y+50);
    color w = color(225,225,225);
    fill(w);
    rect(this.x-7.5, this.y+10, 15, 40);
    color o = color (255,165,0);
    fill(o);
    triangle(this.x-7.5,this.y+50,this.x-3.75,this.y+60,this.x,this.y+50);
    triangle(this.x,this.y+50,this.x+3.75,this.y+60,this.x+7.5,this.y+50);
    //popMatrix();
  }
  
  //move the missile; if it is has hit something move it off screen
  void move(boolean s) {
    display();
    if(s){
      this.y = 0;
    } else {
    this.y -= 20;
    }
  }
  
  //check if the rocket is offscreen
  boolean offscreen(){
    if(this.y <= 0){
      return true;
    }
    return false;
  }
}
