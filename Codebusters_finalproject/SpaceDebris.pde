class SpaceDebris {
  Debris [] d1;
  boolean goingleft;
  float r;
  int n;
  float s;
  int startx;
  //initialize values and populate the array with debris objects
  SpaceDebris(float r, int n, float s){
    this.r = r;
    this.n = n;
    this.s = s;
    d1 = new Debris[this.n];
    for(int i = 0; i < d1.length; i++){
      //the debris is either starting on the right or the left
      float left = floor(random(2));
      //the debris is starting at a random position above the screen
      float y = random(height-150);
      if(left == 0){
        goingleft = false;
        startx = -100;
      } else{
        goingleft = true;
        startx = width+100;
      }
        d1[i] = new Debris(startx, y, this.r, goingleft, s);
    }
  }
   
  //moves debris & and checks if it's offscreen
  void moving(){
    for(int i = 0; i < d1.length; i++){
      d1[i].move(d1[i].left);
      d1[i].offscreen();
    }
  }
  
  //determine if spaceship collides with debris
  Boolean Collision(Spaceship player){
    for(int i = 0; i < d1.length; i++){
      if(abs(player.x - d1[i].x) <= 45 && ((player.y - d1[i].y) <= 30) && (d1[i].y - player.y) <= 95){
        return true;
      }
    }
    return false;
  }
  
  //if the debris has been shot move the debris off screen
  boolean shot(Missile m){
    for(int i = 0; i < d1.length; i++){
      if(abs(m.x - d1[i].x) <= 30 && ((m.y - d1[i].y) <= 30)){
        d1[i].x = startx;
        d1[i].speed += 1;
        return true;
      }
    }
    return false;
  }
  
}
