class HighScores {
  
  //initialize scores
  int [] scores;
  String [] names;
  int score;
  int newScore;
  String [] out;

  int spot;
  Boolean user; 
  int count = 0;
  
  //initialize class
  HighScores(){
    
    scores = new int [5];
    names = new String [5];
    out = new String [5];
    
    //read scores from file
    String [] lines = loadStrings("list.txt");

    for (int i = 0 ; i < lines.length; i++) {
      String [] l = split(lines[i], " ");
      scores[i] = int(l[1]);
      names[i] = l[0];
    }
  }
  
  
  void show(String name, Boolean done){
    
    //game over screen
    fill(176, 83, 203);
    textSize(60);
    text("Game Over", width/2 - 115, height/2 - 250);
    textSize(45);
    text("High Scores", width/2 - 95, height/2 - 100);
    stroke(176, 83, 203);
    line(width/2 - 200, height/2 - 90, width/2 + 170, height/2 - 90);
    stroke(0);

    //player 1 wins
    if(player1.score > player2.score){
      fill(255);
      //player one will be on the high score board
      if (high(player1.score)) {
        textSize(25);
        fill(255);
        text("You reached the high score board!!\n\tEnter Your Name and Press Enter:\n", width/2 - 130, height - 200);
        fill(176, 83, 203);
        text(name, width/2 - 60, 680);
        //let player tye name
        if (done && count == 0){
          count += 1;
        }             
        
        //update score board with name and score
        update(name, player1.score);
        
      //player did not make a high score
      } else {
        count += 1;
      }
      
      fill(255, 0, 0);
      textSize(45);
      text("Player 1 wins!!", width/2 - 115, height/2 -200);
      textSize(20);
    }
    
    //player 2 won
    else if(player1.score < player2.score){
      if (high(player2.score)) {
        textSize(25);
        fill(255);
        text("You reached the high score board!!\n\tEnter Your Name and Press Enter:\n", width/2 - 130, height - 200);
        fill(176, 83, 203);
        text(name, width / 2 - 60, 680);
        
        //enters name
        if (done && count == 0){
          count += 1;
        } 
        
        //update score board
        update(name, player2.score);
        
      //not high enough
      } else {
        count += 1;
      }
      
      textSize(45);
      fill(0, 0, 255);
      text("Player 2 wins!!", width/2 - 115, height/2 - 200);
      textSize(20);
    }
    
    //tie display score board
    else{
      fill(255);
      text("DRAW", width/2 - 65, height/2 - 200);
      textSize(20);
      count = 2;
    }
    
    //display board after name is typed and save into file
    if(count >= 2){
      textSize(35);
      
      for (int i = 0; i < scores.length; i++){
          fill(50 + i*50, 20, 240);
          text(names[i], width/2 - 200, 350 + i * 40);
          text(scores[i], width/2 + 150, 350 + i * 40);
          out[i] = names[i] + " " + scores[i];
       }
       saveStrings("list.txt", out);
    }
  }
  
  
  //check if player made a high score
  boolean high(int score) {
    for (int i = 0; i < scores.length; i++){
      if (score > scores[i]){
        return true;
      }
    }
    return false;
  }


  //update score board
  void update(String nm, int newScore){
    //determine placement
    if (count == 1 ) {
      for (int i = 0; i < scores.length; i++){
        if (newScore > scores[i]){
          spot = i;
          break;
        }
      }
     
     //update all scores by moving rest down
     if (count == 1){
      for (int j = scores.length - 2; j >= spot -1; j--){
        if ( j >= 0){
     
          scores[j+1] = scores[j];
          names[j+1] = names[j];
        }
      }
      scores[spot] = newScore;
      names[spot] = nm;
      count += 1;
     }
    }
  }
}
