//this class allows for buttons to be displayed, highlighted and clicked
class Button{
  float x, y, w, h;
  boolean clicked;
  boolean over;
  boolean active;
  
  Button(float x, float y, float w, float h){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    clicked = false;
  }
  
  void display(){
    rect(this.x, this.y, this.w, this.h);
  }
  
  //check if mouse if over the button and highlight button if it is
  void isOver(){
    if(mouseX > this.x && mouseY > this.y && 
       mouseX < this.x + this.w && mouseY < this.y + this.h && active) {
         over = true;
         fill(119, 35, 172);
       } else{
         over = false;
         fill(255, 230, 0);
       }
       display();
  }
}
