//Sound was borrowed from this website: 
//Music: https://www.bensound.com
//Effect sound is from http://soundbible.com/538-Blast.html
import processing.sound.*;
SoundFile file;
SoundFile file1;

//initialize level buttons
Button easy;
Button medium;
Button hard;
Button continueButton;
Button quitButton;
Button sound;

//initialize debris
SpaceDebris d;

//initialize player
Spaceship player1;
Spaceship player2;
Spaceship welcome1;
Spaceship welcome2;
float upward;
ArrayList<Missile> missiles;

//powerup
PowerUp power1;
PowerUp power2;
int randomexist;

//collisions happened or not
Boolean collision1;
Boolean collision2;
Boolean prev1;
Boolean prev2;

//debris been shot or not
//missiles deployed or not
Boolean missile = false;
Boolean shot = false;

// countdown timer
Timer countdown;
boolean count;
int totalTime;
int minutesPassed;

//is the game still going
boolean isPaused;
boolean gameEnded;

//restart button
Button restart;

//initialize high score board
HighScores board;
Boolean user;
Boolean get;
String name = "";
int letters = 0;
Boolean done = false;

//font from www.fontsquirrrel.com
PFont space;

void setup() { 
  size(800,800);
  smooth();
  strokeWeight(3);
  space = createFont("Space.ttf", 30);
  
  //start board
  board = new HighScores();
  
  //initialize classes
  player1 = new Spaceship(1, width/3 - 30);
  player2 = new Spaceship(2, width - width/3 + 45);
  welcome1 = new Spaceship(1, 650);
  welcome2 = new Spaceship(2, 150);
  upward = 2.5;
  power1 = new PowerUp(player1, width/3 - 30);
  power2 = new PowerUp(player2, width - width/3 + 45);
  easy = new Button(300, 300, 200, 75);
  medium = new Button(300, 400, 200, 75);
  hard = new Button(300, 500, 200, 75);
  missiles = new ArrayList<Missile>();
  //initialize timer in seconds
  totalTime = 120;
  
  //randomly decide a time for the powerup (should be atleast 20 sec before end of game)
  randomexist = int(random((totalTime-20)*1000));
  
  //play sound
  file = new SoundFile(this, "music.mp3");
  file.loop();
  file1 = new SoundFile(this, "Blast.mp3");
  
  //initialize button
  restart = new Button(330, 725, 150, 60);
  sound = new Button (680, 30, 110, 40);
  
  //initialize continue/quit buttons
  continueButton = new Button(300, 350, 200, 75);
  quitButton = new Button(300, 500, 200, 75);
}

void draw(){
  background(0);
   
  //sound button
  sound.active = true;
  sound.isOver();
  fill(0);
  textSize(15);
  text("Sound On/Off", 705, 55);
  
  //beginning screen
  if(easy.clicked == false && medium.clicked == false && hard.clicked == false){
    easy.active = true;
    medium.active = true;
    hard.active = true;
    quitButton.active = false;
    continueButton.active = false;
    restart.active = false;
    user = false;
    countdown = new Timer(totalTime);
    fill(176, 83, 203);
    stroke(176, 83, 203);
    strokeWeight(8.5);
    line(260, 182.75, 455, 182.75);
    line(0, 260, 225, 260);
    line(476, 260, 800, 260);
    //text("Welcome to", 180, 150);
    textFont(space);
    textSize(105);
    text("Space Race", 225, 250);
    noStroke();
    textSize(20);
    fill(255);
    text("Take your rocket upward by dodging the debris.", width/2 - 152, height - 175);
    text("               Make it to the top +5", width/2 - 152, height - 150);
    text("                 Get hit by debris -1", width/2 - 152, height - 125);
    text("      Highest Score after Timer Ends Wins", width/2 - 152, height - 100);
    textSize(30);
    easy.isOver();
    fill(0);
    //textSize();
    text("Easy", 375, 348);
    medium.isOver();
    fill(0);
    //textSize(20);
    text("Medium", 365, 448);
    hard.isOver();
    fill(0);
    //textSize(20);
    text("Hard", 375, 548);
    welcome1.display(false);
    welcome1.powerupstate = true;
    welcome2.display(false);
    welcome2.powerupstate = true;
    welcome1.y -= upward;
    welcome2.y -= upward;
  } 
  
  //paused game state
  else if (isPaused == true){

    textSize(25);
    //buttons
    continueButton.active = true;
    quitButton.active = true;
    easy.active = false;
    medium.active = false;
    hard.active = false;
    restart.active = false;
    user = false;
    
    countdown.pause();
    continueButton.isOver();
    
    fill(255);
    textSize(40);
    text("PAUSED", width * 1/3 + 77, height * 1/3);
    fill(0);
    textSize(30);
    text("Continue", 360, 395);
    quitButton.isOver();
    fill(0);
    textSize(30);
    text("Quit", 377, 545);
    
    
  //game over state
  }
  else if(gameEnded == true){
    //display buttons
    easy.active = false;
    medium.active = false;
    hard.active = false;
    quitButton.active = false;
    continueButton.active = false;
    restart.active = true;
    restart.isOver();
    textSize(30);
    fill(0);
    text("Restart", 370, 765);
    
    //check for high score
    if(board.high(player1.score) || board.high(player2.score) && letters == 0){
      user = true;
      get = true;
    }
    
    fill(255, 65, 100);
    //display high scores
    count = false;
    board.show(name, done);
  }
  
  //game is being played
  else {
  
    easy.active = false;
    medium.active = false;
    hard.active = false;
    quitButton.active = false;
    continueButton.active = false;
    user = false;
    
    //display restart button
    restart.active = true;
    restart.isOver();
    textSize(30);
    fill(0);
    text("Restart", 370, 765);
    
    //display player names and scores
    textSize(70);
    fill(255, 0, 0);
    text(player1.score, 160, 100);
    textSize(20);
    text("Player 1",  width/3 - 58, height - 10);
    fill(0, 0, 255);
    text("Player 2",  width - width/3 + 15, height - 10);
    textSize(70);
    text(player2.score, 590, 100);
    
    
    //display debris
    fill(90,50,80);
    d.moving();
    
    //determine collsions
    prev1 = collision1;
    prev2 = collision2;
    collision1 = d.Collision(player1);
    collision2 = d.Collision(player2);
    
    //determine score from collision
    textSize(70);
    if (prev1 != collision1 | player1.y < 1.2) {
      player1.Score(collision1);
    }
    if (prev2 != collision2 | player2.y < 1.2) {
      player2.Score(collision2);
    }
      
    
    //display timer
    if (countdown.isRunning == false){
      countdown.start();
    }
    
    //compute how much time in seconds and minutes has passed
    int secondsPassed = countdown.getTimePassed();
    secondsPassed = secondsPassed/1000;
    secondsPassed = totalTime - secondsPassed;
    minutesPassed = secondsPassed/60;
    secondsPassed = secondsPassed % 60;
    textSize(45);
    fill(255);
    if (secondsPassed < 10){
      text("Time ", 370, 50);
      textSize(35);
      text(minutesPassed + ":"+ "0" + secondsPassed, 372, 80);
    }
    else{
    //text(" Time\n " + minutesPassed + ":"+ secondsPassed, 370, 50);
      text("Time ", 370, 50);
      textSize(35);
      text(minutesPassed + ":"+ secondsPassed, 375, 80);
    }
    textSize(20);
    text("Press 'p' to pause", 342, 105);
    
    //show powerups at the randomly generated time
    if (countdown.getTimePassed()>= randomexist){
      power1.display();
      power2.display();
    }
    
    //display rockets
    player1.display(collision1);
    player2.display(collision2);
    
    //if missiles are deployed, move, display and check if they hit anything
    if(missile){
      for(Missile m1: missiles){
        shot = d.shot(m1);
        if (shot && file.isPlaying()){
          file1.amp(1.0);
          file1.play();
        }
        m1.move(shot);
      }
    }
    
    // if a missile is off screen delete the object from the array list
    for (int i = missiles.size() - 1; i >= 0; i--) {
      Missile m1 = missiles.get(i);
      if (m1.offscreen()) {
        missiles.remove(i);
      }
    }

    //display rules
    textSize(15);
    text("Use W(up) A(left) \nS(Down) D(Right)\nF(fire)", 30, height - 60);
    text("Use ↑(up) ←(left) \n↓(Down) →(Right)\n/(fire)", width - 150, height - 60);
 
  } 
  
  //end the game when countdown ends
  if(countdown.hasEnded()){
    gameEnded = true;
  }  
    
}

void keyPressed(){
  //if player 1 using WASD
  if (key == 'w' || key == 'W'){
        player1.goUp=true;
      }
  if (key == 's' || key == 'S'){
        player1.goDown=true;
      }
  if (key == 'a' || key == 'A'){
        player1.goLeft=true;
      }
  if (key == 'd' || key == 'D'){
        player1.goRight=true;
      }
  if (key == 'f' || key == 'F'){
        missiles.add(new Missile(player1.x, player1.y));
        missile = true;
  }
      
  //player 2 using arrow keys 
  if (keyCode == UP){
        player2.goUp=true;
      }
  if (keyCode == DOWN){
        player2.goDown=true;
      }
  if (keyCode == LEFT){
        player2.goLeft=true;
      }
  if (keyCode == RIGHT){
        player2.goRight=true;
  }
  if (key == '/'){
        missiles.add(new Missile(player2.x, player2.y));
        missile = true;
  }
  
  //pause
  if ((key == 'p' || key == 'P') && gameEnded != true){
    isPaused = true;
  }
  
  //getting name for high score board
  if(user && get){
    if (key == ENTER){
       letters += 1;
       done = true;
       user = false;
     }
    else if (key == BACKSPACE && name.length() > 0){
       name = name.substring(0, name.length() - 1);
    }
    else {
       name += key;
    }
    get = false;
  }
}


void keyReleased(){
  
   //if player 1 using WASD
  if (key == 'w' || key == 'W'){
        player1.goUp=false;
      }
  if (key == 's' || key == 'S'){
        player1.goDown=false;
      }
  if (key == 'a' || key == 'A'){
        player1.goLeft=false;
      }
  if (key == 'd' || key == 'D'){
        player1.goRight=false;
      }
  //player 2 using arrow keys 
  if (keyCode == UP){
        player2.goUp=false;
      }
  if (keyCode == DOWN){
        player2.goDown=false;
      }
  if (keyCode == LEFT){
        player2.goLeft=false;
      }
  if (keyCode == RIGHT){
        player2.goRight=false;
  } 
  
  //getting name without repeats
  if(user){
    get = true;
  }
}


void mousePressed(){

  //levels buttons-if any button has been clicked all clicked attributes are set to true
  // the amount of debris objects and the speed at which they can travel is different per level
  if(easy.over && easy.active){
    easy.clicked = true;
    medium.clicked = true;
    hard.clicked = true;
    d = new SpaceDebris(30,10,1.5);
  } else if(medium.over && medium.active){
    easy.clicked = true;
    medium.clicked = true;
    hard.clicked = true;
    d = new SpaceDebris(30,20,3);
  } else if(hard.over && hard.active){
    easy.clicked = true;
    medium.clicked = true;
    hard.clicked = true;
    d = new SpaceDebris(30,30,4.5);
  }
  
  //button to pause and play sound
  if(sound.over && sound.active){
    if(file.isPlaying()){
      file.pause();
    } else {
      file.loop();
    }
  }
  
  //restart button
  if(restart.over && restart.active){
    restart.isOver();
    gameEnded = false;
    restart.active = true;
    textSize(50);
    easy.clicked = false;
    medium.clicked = false;
    hard.clicked = false;
    randomexist = int(random((totalTime-20)*1000));
    player1.Score(true);
    player2.Score(true);
    power1.restart();
    power2.restart();
    player1.score = 0;
    player2.score = 0;
    board.count = 0;
    name = "";
    user = false;
    letters = 0;
    done = false;
    get = false;
  }
  
  //pause and quit buttons
  if(continueButton.over && continueButton.active){
    isPaused = false;
    countdown.unpause();
  }
  
  else if(quitButton.over && quitButton.active){
    count = false;
    gameEnded = false;
    isPaused = false;
    easy.clicked = false;
    medium.clicked = false;
    hard.clicked = false;
    player1.Score(true);
    player2.Score(true);
    player1.score = 0;
    player2.score = 0;
    board.count = 0;
  }
  
}
