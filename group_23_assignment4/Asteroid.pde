class Asteroid {
  float initial_x;
  float initial_y;
  
  float x;
  float y;
  
  PShape asteroid;
  PShape aShape;
  color c;
  PVector speed;
  
  public Asteroid(float x, float y, color c, PVector speed){
    this.initial_x=x;
    this.initial_y=y;
    this.x=x;
    this.y=y;
    this.c=c;
    this.speed = speed;
    //create asteroid shape 
    fill(c);
    asteroid = createShape(GROUP);
    aShape = createShape();
    aShape.beginShape();
    aShape.vertex(54, 23);
    aShape.vertex(18, 29);
    aShape.vertex(-38, 26);
    aShape.vertex(-55, 8);
    aShape.vertex(-51, -14);
    aShape.vertex(-18, -21);
    aShape.vertex(7, -30);
    aShape.vertex(55,-24);
    aShape.vertex (57, 6);
    aShape.endShape(CLOSE);
    
    PShape crater=createShape(ELLIPSE,-17, 7, 10,10);
    PShape crater2=createShape(ELLIPSE,37, -10, 20,15);
    PShape crater3=createShape(ELLIPSE,25, 20, 5,5);
    PShape crater4=createShape(ELLIPSE,-10, -20, 5,5);
    
    asteroid.addChild(aShape);
    asteroid.addChild(crater);
    asteroid.addChild(crater2);
    asteroid.addChild(crater3);
    asteroid.addChild(crater4);
  }

  
  void display(){
    
    if (x > width){
      x=initial_x;
      y=initial_y;
    }
    
    
    pushMatrix();
    translate(x,y);
    
    pushMatrix();
    shape(asteroid, 0,0);
    popMatrix();
    
    pushMatrix();
    translate (-100, 0);
    rotate(x/20.0);
    scale(0.1);
    shape(asteroid, 0 ,0);
    popMatrix();
  
    pushMatrix();
    translate(-80,  40);
    rotate(-x/30.0);
    scale(0.3);
    shape(asteroid,0,0);
    popMatrix();
    
    pushMatrix();
    translate(-150, 25);
    rotate(x/25.0);
    scale(0.2);
    shape(asteroid, 0,0);
    popMatrix();
    popMatrix();

    x+=speed.x;
    y+=speed.y;
    
  }
  
  
}
