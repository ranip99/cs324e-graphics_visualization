Satellite sat;
Satellite sat2;
Satellite sat3;
Asteroid a1;
Asteroid a2;
Asteroid a3;
StarSystem star, star2, star3;
float angle = 0;
PImage img;

import processing.sound.*;
SoundFile file;

void setup() {
  img = loadImage("sky.jpg");
  surface.setSize(img.width, img.height);
  
  file = new SoundFile(this, "music.mp3");
  file.play();
  
  PVector p = new PVector (1.5,0.3);
  a1 = new Asteroid (20,200, 120,p);
  PVector s = new PVector (2, -0.5);
  a2 = new Asteroid (10, 400,70,s);
  PVector b = new PVector(2.5, .7);
  a3 = new Asteroid (5, 10, 170, b);
  
  sat = new Satellite(img.width/2 + 100, img.height/2, .06, 8, color(45, 86, 200));
  sat2 = new Satellite(img.width/4, img.height/2, -.03, 14, color(124, 175, 132));
  sat3 = new Satellite(700, 300, -0.05, 10, color(200, 154, 100));
  
  star = new StarSystem(0, 0, color(200, 200, 0));
  star2 = new StarSystem(0, 150, color(150, 175, 200));
  star3 = new StarSystem(600, 0, color(230, 150, 20));
  
}

void draw() {
  
  background(img);
  a1.display();
  a2.display();
  pushMatrix();
  scale(0.7);
  a3.display();
  popMatrix();
  
  sat.display();
  sat2.display();
  sat3.display();
  
  star.display();
  pushMatrix();
  translate(10, 10);
  scale(1.5);
  rotate(angle);
  star2.display();
  popMatrix();
  pushMatrix();
  scale(1.2);
  star3.display();
  popMatrix();

}
