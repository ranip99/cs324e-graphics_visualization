class StarSystem {
  
  float x;
  float y;
  float angle = 0;
  
  
  //float speed;
  color c;

  
  StarSystem(float x, float y, color c) {
    this.x = x;
    this.y = y;
   
    this.c = c;
    //this.angle = angle;
  }
  
  void display(){
    //beginShape();
    //vertex(x, y + 50);
    //vertex(x + 10, y + 10);
    //vertex(x + 50, y);
    //vertex(x + 10, y - 10);
    //vertex(x, y - 50);
    //vertex(x - 10, y - 10);
    //vertex(x - 50, y);
    //vertex(x - 10, y + 10);
    //endShape();
    //noStroke();
    
    fill(c);
    
    pushMatrix();
    translate(x, y);
    rotate(angle);
    scale(.5);
    beginShape();
    vertex(0, 70);
    //vertex(10, 10);
    vertex(10, 20);
    vertex(30, 30);
    vertex(20, 10);
    vertex(70, 0);
    //vertex(10, -10);
    vertex(20, -10);
    vertex(30, -30);
    vertex(10, -20);
    vertex(0, -70);
    //vertex(-10, -10);
    vertex(-10, -20);
    vertex(-30, -30);
    vertex(-20, -10);
    vertex(-70, 0);
    //vertex(-10, 10);
    vertex(-20, 10);
    vertex(-30, 30);
    vertex(-10, 20);
    endShape();
    noStroke();
    pushMatrix();
    
    translate(50, 50);
    rotate(angle);
    scale(.3);

    beginShape();
    vertex(0, 50);
    vertex(10, 10);
    vertex(50, 0);
    vertex(10, -10);
    vertex(0, -50);
    vertex(-10, -10);
    vertex(-50, 0);
    vertex(-10, 10);
    endShape();
    
    pushMatrix();
    translate(50, 50);
    rotate(angle);
    scale(.75);
    //ellipse(10, 10, 100, 100);
    beginShape();
    vertex(0, 50);
    vertex(10, 10);
    vertex(50, 0);
    vertex(10, -10);
    vertex(0, -50);
    vertex(-10, -10);
    vertex(-50, 0);
    vertex(-10, 10);
    endShape();
    popMatrix();
    popMatrix();
    popMatrix();
    
    
    
    
    x += .2;
    y += .4;
    angle += .04;
    
    if (x >= width) {
      x = 0;
    }
    if (y >= height) {
      y = 0;
    }
    
  

  
  }
}
