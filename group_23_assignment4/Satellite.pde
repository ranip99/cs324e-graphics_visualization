class Satellite {
  
  float x;
  float y;
  color c;
  float angle;
  float size;
  float ang = 1;
  
  Satellite(float x, float y, float angle, float size, color c) {
    this.x = x;
    this.y = y;
    this.c = c;
    this.size = size;
    this.angle = angle;
  }
  
  void display() {
    ellipseMode(CENTER);
    rectMode(CENTER);
    pushMatrix();
    translate(x, y);
    rotate(-ang/4);
    
    pushMatrix();
    noStroke();
    translate(150, 0);
    rotate(-ang/2);
    fill(180);
    ellipse(0, 0, size, size);
    triangle(0, -size + size/2, size +5, 0, 0, size -size/2 );
    
    pushMatrix();
    stroke(5);
    rotate(ang*2);
    translate(size + size * 1.5, 0);
    //rotate(angle/3);
    fill(c);
    rect(0, 0, size, size);
    rect(size, 0, size, size);
    rect(0, size, size, size);
    rect(size, size, size, size);
    rect(size * 2, 0, size, size);
    rect(size * 2, size, size, size);
    popMatrix();
    
    pushMatrix();
    rotate(ang*2);
    translate(-size - size * 3.5, 0);
    //rotate(angle/3);
    fill(c);
    rect(0, 0, size, size);
    rect(size, 0, size, size);
    rect(0, size, size, size);
    rect(size, size, size, size);
    rect(size * 2, 0, size, size);
    rect(size * 2, size, size, size);
    popMatrix();
    
    popMatrix();
    popMatrix();
    
    ang += angle;
  }
}
