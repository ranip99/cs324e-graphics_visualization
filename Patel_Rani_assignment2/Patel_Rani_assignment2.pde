PImage img;
PImage img2;
float [][] gkernel = {{0.0625, .125, 0.0625}, 
                      {.125, .25, .125}, 
                      {0.0625, .125, 0.0625}};
float [][] ehkernel = {{-1, 0, 1}, 
                      {-2, 0, 2}, 
                      {-1, 0, 1}};
float [][] evkernel = {{-1, -2, -1}, 
                      {0, 0, 0}, 
                      {1, 2, 1}};

void setup() {
  
  surface.setResizable(true);
  img = loadImage("bird.png");
  surface.setSize(img.width, img.height);
  img2 = loadImage("bird.png");
  //img2 = createImage(width, height, ARGB);
  //copy(img, 0, 0, img.width, img.height, 0, 0, img2.width, img2.height);
  
}

void draw() {
  
  loadPixels();
  img.loadPixels();
  image(img2, 0, 0);

}

void original() {
  
   for (int x = 1; x < img.width; x++) {
    for (int y = 1; y < img.height; y++) {
      int index = x + y * img.width;
       img2.pixels[index] = color(red(img.pixels[index]), green(img.pixels[index]), blue(img.pixels[index]));
    }
   }
   img2.updatePixels();
   image(img2, 0, 0);
  
}

void keyPressed() {
  
  if (key == '0') {
    original();
   } else if (key == '1') {
      grayscale();
   } else if (key == '2') {
      contrast();
   } else if (key == '3') {
      gaussian();
   } else if (key == '4') {
      edge();
   } else if (key == '5') {
     filter();
   }
}

void grayscale() {
  
  for (int x = 1; x < img.width; x++) {
    for (int y = 1; y < img.height; y++) {
      int idx = x + y * img.width;
      img2.pixels[idx] = color((red(img.pixels[idx]) + green(img.pixels[idx]) + blue(img.pixels[idx])) / 3);
    }
  }
  img2.updatePixels();
  image(img2, 0, 0);
  
}

void contrast() {
  
  colorMode(HSB, 360, 100, 100);
  for (int x = 1; x < img.width; x++) {
    for (int y = 1; y < img.height; y++) {
      int idx = x + y * img.width;
      float brightness = brightness(img.pixels[idx]);
      
      if (brightness > 60) {
        brightness = brightness + 12;
        brightness = constrain(brightness, 0, 100);
      } else if (brightness < 40) {
        brightness = brightness - 12;
        brightness = constrain(brightness, 0, 100);
      }
      
      img2.pixels[idx] = color(hue(img.pixels[idx]), saturation(img.pixels[idx]), brightness);
    }
  }
  img2.updatePixels();
  image(img2, 0, 0);
  colorMode(RGB, 255, 255, 255);
}

void gaussian() {
  
  for (int x = 1; x < img.width -1; x++) {
    for (int y = 1; y < img.height -1; y++) {
      int red = 0; 
      int green = 0;
      int blue = 0;
      
      for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
          int index = (x + i - 1) + (y + j -1) * img.width;
          red += red(img.pixels[index]) * gkernel[i][j];
          green += green(img.pixels[index]) * gkernel[i][j];
          blue += blue(img.pixels[index]) * gkernel[i][j];
        }
      }
      red = constrain(abs(red), 0, 255);
      green = constrain(abs(green), 0, 255);
      blue = constrain(abs(blue), 0, 255);
      
      img2.pixels[x + y * img.width] = color(red, green, blue);
    }
  }
  img2.updatePixels();
  image(img2, 0, 0);
}

void edge() {
  
    for (int x = 1; x < img.width -1; x++) {
      for (int y = 1; y < img.height -1; y++) {
        float red = 0; 
        float green = 0;
        float blue = 0;
        float hred = 0;
        float vred = 0;
        float hgreen = 0;
        float vgreen = 0;
        float hblue = 0;
        float vblue = 0;
      
        for (int i = 0; i < 3; i++) {
          for (int j = 0; j < 3; j++) {
            int index = (x + i - 1) + (y + j -1) * img.width;
            hred += red(img.pixels[index]) * ehkernel[i][j];
            hgreen += green(img.pixels[index]) * ehkernel[i][j];
            hblue += blue(img.pixels[index]) * ehkernel[i][j];
            
            vred += red(img.pixels[index]) * evkernel[i][j];
            vgreen += green(img.pixels[index]) * evkernel[i][j];
            vblue += blue(img.pixels[index]) * evkernel[i][j];
            
          }
        }
        
        red = sqrt(sq(vred) + sq(hred)); 
        green = sqrt(sq(vgreen) + sq(hgreen)); 
        blue = sqrt(sq(vblue) + sq(hblue)); 
        
        red = constrain(abs(red), 0, 255);
        green = constrain(abs(green), 0, 255);
        blue = constrain(abs(blue), 0, 255);
      
        img2.pixels[x + y * img.width] = color(red, green, blue);
      }
    }
    img2.updatePixels();
    image(img2, 0, 0);
}

//extra credit
void filter() {
  
  colorMode(HSB, 360, 100, 100);
  for (int x = 1; x < img.width; x++) {
    for (int y = 1; y < img.height; y++) {
      int idx = x + y * img.width;
      float hue = hue(img.pixels[idx]);
      if (hue > 180) {
        hue = hue + 80;
      } else {
        hue = hue + 20;
      }
        
      hue = constrain(hue, 0, 360);
      img2.pixels[idx] = color(hue, saturation(img.pixels[idx]), brightness(img.pixels[idx]));
    }
  }
  img2.updatePixels();
  image(img2, 0, 0);
  colorMode(RGB, 255, 255, 255);
}
