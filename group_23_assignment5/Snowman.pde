class Snowman {
  float x;
  float y;
  float z;
  float rotation;
  float bounce;
  float position;
  float travel;
  PShape snowman;
  //PImage texture = loadImage("Snowman_01.mtl");
  
  Snowman(float x, float y, float z, float walk, float distance) {
    this.x = x;
    this.y = y;
    this.z = z;
    rotation = 0;
    position = 0;
    bounce = walk;
    travel = distance;
    snowman = loadShape("Snowman_01.obj");
    //snowman.setTexture(texture);
    
  }
  
  void display() {
    
    
    pushMatrix();
    translate(x, y, z);
    scale(.5);
    rotateY(radians(45));
    rotateX(radians(90 + rotation/4));
    translate(0, position, 0);
    //texture(255);
    shape(snowman, 0, 0);
    
    
    pushMatrix();
    translate(position * 1.5, position - 50, 0);
    fill(255,0, 0);
    noStroke();
    rotateZ(radians(-30));
    rotateX(radians(rotation * 2));
    scale(.6);
    shape(snowman, 0, 0);
    popMatrix();
    
    popMatrix();
    
   
    
    
  
    rotation += bounce;
    if (rotation > 2) {
      bounce *= -1;
    }
    if (rotation < -2) {
      bounce *= -1;
    }
    
    if (position > 700) {
      position = -width;
    }
    position += travel;
  }
}
