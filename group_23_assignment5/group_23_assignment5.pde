
float x;
PShape tree;
PShape gift;
Tree a1;
Snowflake sn1;
Snowman snowdad;

void setup() {
  
  size(500, 500, P3D);
  camera(0, 0, width, width, height, 0, 0, 1, 0);
  sn1 = new Snowflake(100, 0.2);
  snowdad = new Snowman(width/2, height * 2/3, 0, 1, 5);
  a1= new Tree();
}

void draw() {
  
  background(0);
  lights();
  
  sn1.display();
  snowdad.display();
  a1.display();
  
}
