class Snowflake {
  
  float pos;
  float rot;
  PShape snow = loadShape("snow.obj");
  PShape drop = loadShape("drop.obj");
  
  
  Snowflake(float pos, float rot) {
    this.pos = pos;
    this.rot = rot;
    snow.scale(8, 8, 8);
    drop.scale(0.3, 0.3, 0.3);
  }
  
  void display() {
    
    pushMatrix();
    translate(pos, pos, pos*2);
    rotateY(rot);
    rotateX(1.75);
    
    directionalLight(100, 215, 255, pos, pos, pos);
    ambientLight(89, 213, 255, pos, pos, pos);
    
    shape(snow, 0, 0, snow.width, snow.height);
    
    pushMatrix();
    translate(0, 0, -10);
    rotateZ(rot*3);
    

    shape(drop, 3, -15, drop.width, drop.height);
    shape(drop, -15, 3, drop.width, drop.height);
    shape(drop, 10, 10, drop.width, drop.height);
    
    if (pos >= width/2){
      pos = 60;
    }
    
    popMatrix();
    popMatrix();
    
    rot+= 0.1;
    pos += 2;
    
  }
}
  
