class Tree {
  float scalex;
  boolean bigger;
  PShape tree;
  PShape gift;
  PShape gift2;
  Tree (){
    tree = loadShape ("tree.obj");
    tree.scale (5,5,5);
    gift = loadShape ("box.obj");
    gift2 = loadShape ("box.obj");
    scalex= 0.0;
    bigger = true;

  }
  
  void display(){
    translate(400,600,100);
    rotateX(PI);
    rotateY(x/20.0);
    pointLight(255, 246, 75 ,0, 150, 0);
    shape (tree, 0, 0, tree.width, tree.height);
    
    
    //gift 1
    pushMatrix();
    translate (50, 0, 50);
    rotateX(3* PI/2);
    scale(0.05*scalex);
    shape (gift, 0, 0, gift.width, gift.height);
    popMatrix();
    
    
    //gift 2
    pushMatrix();
    translate (-50, 0, -50);
    rotateX(3* PI/2);
    scale(0.05*scalex);
    shape (gift, 0, 0, gift.width, gift.height);
    popMatrix();
    
    
    x++;
    
    if (scalex >= 40){
      bigger = false;
      scalex--;
    }
    else if (scalex <= 0){
      bigger = true;
      scalex++;
    }
     else if (bigger) {
       scalex++;
     }
     else if (bigger == false ){
       scalex--;
     }
    
  }
}
