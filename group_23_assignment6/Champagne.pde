class Champagne {
  float x,y;
  PShape bottle;
  ArrayList<Bubble> bubbles;
  Champagne (float _x, float _y){
    x = _x;
    y= _y;
    bottle= loadShape("bottle.svg");
    //fill array list with objects from particle class
    bubbles = new ArrayList<Bubble>();
  }

  void display(){
    pushMatrix();
    shapeMode(CENTER);
    translate(x,y);
    scale(0.08);
    rotate(-PI/5);
    //change color of bottle
    bottle.disableStyle();
    fill(37,70,54);
    shape(bottle, 0,0);
    popMatrix();
    //bub is one bubble in the bubbles array 
    for (Bubble bub : bubbles){
      bub.applyForces();
      bub.display();
    }
  }
  
  void addBubble(){
    bubbles.add(new Bubble(x-35,y-50));

  }
    
}
