class Dancer {
  
  //initialize certain variables
  Spring [] points = new Spring[10];
  float gravity = 15.0;
  
  int posx = 20;
  int posy = 400;
  
  boolean changex = true;
  boolean changey = true;
  
  PImage handl, handr;

  Dancer() {
    
    
    //new Spring class points
    for (int i = 0; i < points.length; i++) {
      points[i] = new Spring(width/3, i*(height/points.length));
    }
    
    //load and resize images for l and r hand
    handl = loadImage("handl.png");
    handl.resize(handl.width/12, handl.height/12);
    
    handr = loadImage("handr.png");
    handr.resize(handr.width/12, handr.height/12);
   
  }


  void display() {

     // if pointer for movement of person goes out of bounds change direction
    if (posx < 0) {
      changex = true;
    } else if (posx > 300 ) {
      changex = false;
    }
    
    // randomly move pointer
    if (changex == true) {
      posx += int(random(2,15));
    } else {
      posx -= int(random(2,15));
    }
    
    //same for y
    if (posy > 400) {
      changey = true;
    } else if (posy < 0) {
      changey = false;
    }
    
    if (changey == true) {
      posy -= int(random(2,10));
    } else {
      posy += int(random(2,10));
    }
    
    
    // place first object
    points[0].setPosition(posx, posy);
    points[0].display(posx, posy);
    strokeWeight(50);
    ellipse(posx, posy, 50, 50);
    strokeWeight(2);
    
    // have everyother object follow the first
    for (int i = 1; i < points.length ; i++) {
      
      //previous positions
      float tempx = points[i-1].x;
      float tempy = points[i-1].y;
      
      points[i].setPosition(tempx, tempy);
      points[i].applyForces(-7, gravity);
      points[i].display(tempx, tempy);
      
      //add the arms to this point
      if (i == 3) {
        strokeWeight(30);
        //draw hands and arms
        line(tempx, tempy, tempx + 100, tempy + 90);
        image(handr, tempx + 90, tempy + 50);
        line(tempx, tempy, tempx - 100, tempy - 30);
        image(handl, tempx - 170, tempy - 70);
        strokeWeight(2);
      }
    }
  }
}
