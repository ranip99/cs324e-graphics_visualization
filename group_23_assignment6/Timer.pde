class Timer{
  int animationTimer, animationTimerValue, currentFrame, numFrames;
  boolean on;
  
  Timer(int speed) {
    animationTimer = 0;
    animationTimerValue = speed;
    currentFrame = 0;
    on = true;
  }
  
  //method returns true if the timer is on and the interval has passed
  //false if timer is not on or if the interval has not passed
  boolean Timing(boolean on) {
    if (on == true){
      if ((millis() - animationTimer) >= animationTimerValue) {
        animationTimer = millis();
        return true;
      }
      return false;
    }
    return false;
   }//end method
     
}//end class
