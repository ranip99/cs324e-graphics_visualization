class Balloon{
  // initializing objects to these points
  float pX;
  float pY;
  // spring variables used
  float y_spring = 100; 
  float ry_spring = 20;
  float vy_spring;
  float ks = 0.05;
  float kd = 0.2;
  // other important variables
  float air_force = 0.2;  
  float speed = 0.4;
  float mass = 15;
  
  Balloon(float x, float y){
    pX = x;
    pY = y;
  }
 
  void move() {
    // set y axis to spring
    float pY2 = pY + y_spring;
    
    // make the balloon components
    PShape balloon = createShape(GROUP);
    PShape strand = createShape();
    strand.beginShape();
    strand.strokeWeight(2);
    strand.stroke(255);
    strand.vertex(pX, pY2);
    strand.vertex(pX, pY2 + 40);
    strand.endShape();
    PShape oon = createShape();
    oon.beginShape();
    oon.vertex(pX, pY2);
    oon.vertex(pX - 6, pY2 + 35);
    oon.vertex(pX + 6, pY2 + 35);
    oon.endShape();
    PShape ball = createShape(ELLIPSE, pX, pY2 , 50, 60);
    // put them together and color
    balloon.addChild(strand);
    balloon.addChild(oon);
    balloon.addChild(ball);
    shape(balloon);
    fill(255, 30, 30);
   
    // add speed to where it is in the x axis; floating effect
    pX = pX + speed;
    // apply spring code to y axis
    float force = -((ks * (y_spring - ry_spring)) + kd * vy_spring);
    float accel = force/mass;
    vy_spring = vy_spring + accel;
    y_spring = y_spring + vy_spring;
    // multiply  air resistance  to speed
    speed *= air_force;

    // let balloon move across x axis
    if (pX > 0) {
      pX ++;
    }
    // let balloon just "hover" below the "ceiling"
    if (speed == 0){
      pY --;
        if (pY  < 0){
          pY = 0;
        }
    }
  }    
}
