class Spring {
  
  float x, y;
  float vx, vy;
  float rx, ry;

  Spring(float rx, float ry) {
    this.rx = rx;
    this.ry = ry;
  }
  
 //position is always x and y which changes through apply forces
 void setPosition(float x, float y) {
   this.x = x;
   this.y = y;
 }
 
 void applyForces(float hForce, float vForce) {
   
   //spring constant ks, kd = 0.1
   //calculate the horixontal force
   float fx = -((.1 * (x - rx)) + .1 * vx) + hForce;
   //calculate velocity and x position 
   vx += fx / 2;
   x += vx;
   
   //calculate y velocity and y position from vertical force
   float fy = - ((.1 * (y - ry)) + .1 * vy) + vForce;
   vy += fy / 2;
   y += vy;
 }
 
 void display(float x1, float x2) {
   // draw body
   strokeWeight(50);
   line(x, y, x1, x2);
   strokeWeight(2);
 }
 
}
