//make instances for objects
Dancer p;
Floor f;
Timer t;
Champagne bottle1;
Balloon balloon;

//colors set for background
float r = 100, g = 200, b = 250;

//boolean for slow-motion
boolean slow = false;

void setup () {
  
  size(500, 500);
  
  bottle1 = new Champagne (450, 250);
  balloon = new Balloon(0, height * 1/4);
  
  //new dancer
  p = new Dancer();
  
  //create a timer and the floor
  t = new Timer(200);
  f = new Floor();

}

void draw() {
  
  //change the background every 15 frames
  background(r, g, b);
  if (frameCount % 15 == 0) {
    r = random(1,255);
    g = random(1,255);
    b = random(1,255);
  }
  
  //check if the timer is on play and if the interval is reached
  boolean play = t.Timing(t.on);
  
  //if the interval is reached create the future floor pattern
  if(play == true){
    f.futureGrid();
  }
  
  //display the colored floor
  f.colorGrid();
  
  balloon.move();
  
  //display dancer
  p.display();
  
  bottle1.addBubble();
  bottle1.display();

  // slows down motion 
  if (slow == true) {
    frameRate(8);
  } else {
    frameRate(60);
  }

}

void keyPressed(){
  //if p is pressed pause/play the timer and set the on attribute to its opposite
  if(key == 'p'){
    t.on = !t.on;
  }
  
  //if s is pressed slow motion activated
  if (key == 's') {
      slow = !slow;
   }
   
  //reset the ballon if r is pressed
  if (key == 'r') {
      balloon = new Balloon(0, height * 1/4);
  }
   
   
}
  
