class Hexagon {
  float x, y, r;
  float angle;
  
  Hexagon(float x, float y, float r) {
    this.x = x;
    this.y = y;
    this.r = r;
    angle = 360/6;
  }
  
  //creates a hexagon shape
  void display(){
    beginShape();
    for(int i = 0; i < 6; i++){
      vertex(x + r * cos(radians(angle*i)), y + r * sin(radians(angle*i)));
      //println(x + r * cos(radians(angle*i)), y + r * sin(radians(angle*i)));
    }
    endShape(CLOSE);
  }
}
