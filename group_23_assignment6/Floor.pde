class Floor {
  int cellSize = 20;
  
  //number of cells fitting within the width and height dimensions
  int countX = 300/cellSize;
  int countY = 200/cellSize;

  //cell and cell Buffer grid
  int[][] cells; 
  int[][] cellsBuffer; 
  
  //array of hexagon objects to create the floor
  Hexagon [] [] grid;
  
  // an array of colors to fill the floor
  color [] colors = {color(255, 0,0), color(255, 159, 0), color(223, 255, 0),
                     color(0, 255, 0), color(0, 17, 255), color(204, 0, 255)};
  
  Floor () {
    //initialize the arrays to the correct number of cells
    cells = new int[countX][countY];
    cellsBuffer = new int[countX][countY];
    grid = new Hexagon[countX][countY];
   
    //initialize states
    for (int x=0; x < countX; x++) {
      for (int y=0; y < countY; y++) {
        
        //each cell can have a value between 0 & 5
        float state = floor(random(6)); 
        
        //populate the cell array & save the state
        cells[x][y] = int(state); 
        
      }//end j loop
    }//end i loop
  }//end constructor
  
  //populates the grid array with Hexagon objects
  void grid() {
    for(int i = 0; i < countX; i++){
      for(int j = 0; j < countY; j++){
        
        //every other cell will start a the width between cells
        if(j % 2 == 0){
          grid[i][j] = new Hexagon((3* cellSize * (i)), (0.8*cellSize * (j+22)), cellSize);
        } else {
          grid[i][j] = new Hexagon((3* cellSize * (i+.5)), (0.8*cellSize * (j+22)), cellSize);
       
        }//end else
      }//end j loop
    }//end i loop
  }// end method

    //method returns the number of neighbors with the same value
    int neighbors(int i, int j) {
      int sameNeighbors = 0;
      for(int nx = i-1; nx < i+2; nx++){
        for(int ny = j-1; ny < j+2; ny++){
          
          //stay within the boundaries and don't check the cell itself
          if(nx > 0 && nx < countX && nx != i &&
             ny > 0 && ny < countY && ny != j){
            if(cells[nx][ny] == cells[i][j]){
              
              //if the neighboring cell has the same value increment the neighbor count
              sameNeighbors ++;
              
            }//end if
             }//end if
        }//end j loop
      }//end i loop  
      
      return sameNeighbors;
      
    }//end method
    
    //method sets future values based on current grid and updates current grid
    void futureGrid(){
      for(int i = 0; i < countX; i++){
        for(int j = 0; j < countY; j++){
           //get the number of neighbors with the same value
           int value = neighbors(i, j);
           
            //edges can only have a value between 0 & 5
            if(i == 0|| i == countX-7 ||j == 0|| j == countY-1){
              cellsBuffer[i][j] = (cells[i][j]+1) % 6;
              
            //if a cell has a value of more than 5 and no neighbors with the same value
            //keep it the same value
            } else if (value == 0 && cells[i][j] > 5){
              cellsBuffer[i][j] = cells[i][j];
            
            //if a cell has a value less than or equal to 5 and one or more neighbors with
            //the same value add one to the value
            }else if (value > 0 && cells[i][j] < 6){
              cellsBuffer[i][j] = cells[i][j]+1;
           
            //if a cell has a value less than or equal to 5 and no neighbors with
            //the same value keep it the same value;
            } else if(value == 0 && cells[i][j] < 6){
              cellsBuffer[i][j] = cells[i][j];
            
            //if a cell has a value of more than 5 and neighbors with the same value
            //set it to a random value between 0 & 5
            } else {
              cellsBuffer[i][j] = floor(random(6));
            }//end else
        }//end j loop
      }//end i loop
      
      //update the cells from the buffer
      for(int i = 0; i < countX; i++){
        for(int j = 0; j < countY; j++){
          cells[i][j] = cellsBuffer[i][j];
        
        }//end j loop
      }//end i loop
    }// end method
    
    //method designates the fill for each of the cells
    void colorGrid(){
      
      //call grid to populate array of hexagons
      grid();
      for(int i = 0; i < countX; i++){
        for(int j = 0; j < countY; j++){
          
          //index the color array based on the value
          //if value is greater than array color it grey
          stroke(0);
          if (cells[i][j] > 5) {
            fill(180);
          } else{
            fill(colors[cells[i][j]]);
          }
          
          //call the hexagon display method to draw the shape
          grid[i][j].display();
        
        }//end j loop
      }//end i loop
    }//end method
}//end class
