//allows for a timer
class Timer {
  
  int startTime;
  int endTime;
  boolean isRunning;
  int timePassed;
  

  Timer(int seconds){
    setTotalTime(seconds);
    isRunning = false;
  }
  
  //start timer at 0
  void start(){
    startTime = millis();
    isRunning =  true;
  }

  //allow a endtime to be set
  void setTotalTime(int seconds){
    endTime = seconds * 1000;
    isRunning = false;
  }
  
  //checks if timer has ended
  boolean hasEnded(){
    timePassed = millis() - startTime;
    if (timePassed >= endTime && isRunning){
      isRunning = false;
      return true;
    }
    else{
      return false;
    }
  }
  
  int getTimePassed(){ 
    timePassed = millis() - startTime;
    return timePassed;
  }
  
}
