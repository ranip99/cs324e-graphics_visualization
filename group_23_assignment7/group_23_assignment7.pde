//initialize debris
SpaceDebris d;

//initialize player
Spaceship player1;
Spaceship player2;

//collisions happened or not
Boolean collision1;
Boolean collision2;
Boolean prev1;
Boolean prev2;

// countdown timer
Timer countdown;
boolean count;
int totalTime;
int minutesPassed;

boolean gameEnded = false;

//restart button
Button restart;

void setup() { 
  size(800,800);
  smooth();
  strokeWeight(3);
    
  //initialize classes
  player1 = new Spaceship(1, width/3 - 30);
  player2 = new Spaceship(2, width - width/3 + 45);
  
  //initialize timer in seconds
  totalTime = 120;
  countdown = new Timer(totalTime);
  
  //initialize button
  restart = new Button(345, 725, 150, 60);
  
  //initialize debris
  d = new SpaceDebris(30,20,3);

}

void draw(){
  background(0);
  
  //display restart button
  restart.active = true;
  restart.isOver();
  textSize(25);
  fill(0);
  text("Restart", 375, 760);

  
  //game over state
  if(gameEnded == true){
    fill(255, 0, 0);
    textSize(45);
    text("Game Over", width/2 - 120, height/2 - 50);
    count = false;
    if(player1.score > player2.score){
      fill(255);
      text("PLAYER 1 WINS!!", width/2 - 165, height/2 + 50);
    }
    else if(player1.score < player2.score){
      fill(255);
      text("PLAYER 2 WINS!!", width/2 - 165, height/2 + 50);
    }
    else{
      fill(255);
      text("DRAW!!", width/2 - 70, height/2 + 50);
    }
  }
    
  else {
    
    //game screen with instructions
    textSize(40);
    fill(140, 20, 255);
    text("Welcome to Space Race!", 145, 40);
    
    fill(255);
    textSize(15);
    text("Dodge the debris", width/2 - 65, height - 150);
    text("                Make it to the top +5", width/2 - 150, height - 130);
    text("                Get hit by debris -1", width/2 - 150, height - 110);
    text("     Highest Score after Timer Ends Wins!", width/2 - 150, height - 90);
  
    //display player names and scores
    textSize(70);
    fill(255, 0, 0);
    text(player1.score, 100, 100);
    textSize(20);
    text("Player 1",  width/3 - 65, height - 10);
    fill(0, 0, 255);
    text("Player 2",  width - width/3 + 15, height - 10);
    textSize(70);
    text(player2.score, 600, 100);
    
    
    //display debris
    fill(90,50,80);
    d.m();
  
    //determine collsions
    prev1 = collision1;
    prev2 = collision2;
    collision1 = d.Collision(player1);
    collision2 = d.Collision(player2);
    
    //determine score from collision
    textSize(70);
    if (prev1 != collision1 | player1.y < 1.2) {
      player1.Score(collision1);
    }
    if (prev2 != collision2 | player2.y < 1.2) {
      player2.Score(collision2);
    }
  
    //display timer
    if (countdown.isRunning == false){
      countdown.start();
    }
    
    //compute how much time in seconds and minutes has passed
    int secondsPassed = countdown.getTimePassed();
    secondsPassed = secondsPassed/1000;
    secondsPassed = totalTime - secondsPassed;
    minutesPassed = secondsPassed/60;
    secondsPassed = secondsPassed % 60;
    textSize(35);
    fill(255);
    if (secondsPassed < 10){
      text("Timer\n " + minutesPassed + ":"+ "0" + secondsPassed, width/2 - 70, 75);
    }
    else{
    text("Timer\n " + minutesPassed + ":"+ secondsPassed, width/2 - 70, 75);
    }
    
    //display rockets
    player1.display(collision1);
    player2.display(collision2);
    
    //display rules
    textSize(15);
    text("Use W(up) A(left) \nS(Down) D(Right)", 30, height - 40);
    text("Use ↑(up) ←(left) \n↓(Down) →(Right)", width - 150, height - 40);
   
  } 
  
  //end the game when countdown ends
  if(countdown.hasEnded()){
    gameEnded = true;
  }  
    
}

void keyPressed(){
  //if player 1 using WASD
  if (key == 'w' || key == 'W'){
        player1.goUp=true;
      }
  if (key == 's' || key == 'S'){
        player1.goDown=true;
      }
  if (key == 'a' || key == 'A'){
        player1.goLeft=true;
      }
  if (key == 'd' || key == 'D'){
        player1.goRight=true;
      }
      
  //player 2 using arrow keys 
  if (keyCode == UP){
        player2.goUp=true;
      }
  if (keyCode == DOWN){
        player2.goDown=true;
      }
  if (keyCode == LEFT){
        player2.goLeft=true;
      }
  if (keyCode == RIGHT){
        player2.goRight=true;
  } 
  
    
}


void keyReleased(){
    //if player 1 using WASD
  if (key == 'w' || key == 'W'){
        player1.goUp=false;
      }
  if (key == 's' || key == 'S'){
        player1.goDown=false;
      }
  if (key == 'a' || key == 'A'){
        player1.goLeft=false;
      }
  if (key == 'd' || key == 'D'){
        player1.goRight=false;
      }
  //player 2 using arrow keys 
  if (keyCode == UP){
        player2.goUp=false;
      }
  if (keyCode == DOWN){
        player2.goDown=false;
      }
  if (keyCode == LEFT){
        player2.goLeft=false;
      }
  if (keyCode == RIGHT){
        player2.goRight=false;
  } 
  
}

void mousePressed(){
  restart.isOver();
  //restart button
  if(restart.over){
    gameEnded = false;
    restart.active = true;
    textSize(50);
    player1.Score(true);
    player2.Score(true);
    player1.score = 0;
    player2.score = 0;
    countdown = new Timer(totalTime);
    d = new SpaceDebris(30,10,1.5);
  }
}
