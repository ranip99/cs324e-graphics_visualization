class Spaceship {
  int score;
  int player;
  float y = height-100.0;
  float initial_x, initial_y;
  float x;
  PShape spaceship;
  boolean goUp, goDown, goRight, goLeft;
  
  Spaceship(int player, float x){
    this.score = 0;
    this.player = player;
    this.x =x;
    this.initial_x = x;
    this.initial_y = y;
    this.goUp = false;
    this.goDown = false;
    this.goRight = false;
    this.goLeft = false;
    //each player has a different spaceship design
    if (player==1){
      //drawn from tip of spaceship (origin is at tip)
      spaceship = createShape(GROUP);
      fill(255,0,0);
      PShape tip = createShape(TRIANGLE, 0,0,-25, 30, 25, 30);
      fill(210,216,219);
      PShape body = createShape(RECT, -25,30,50,80);
      fill(133,153, 162);
      PShape windowframe = createShape(ELLIPSE, 0, 60, 40, 40);
      fill(136,211, 245);
      PShape window = createShape(ELLIPSE, 0, 60, 30, 30);
      fill(255,0,0);
      PShape fin1 = createShape(QUAD, -25, 85, -25, 110, -35, 150, -40, 90);
      PShape fin2 = createShape(QUAD, 25, 85, 25, 110, 35, 150, 40, 90);
      PShape fin3 = createShape(QUAD, 0, 85, 5, 90, 0, 150, -5, 90);
      spaceship.addChild(tip);
      spaceship.addChild(body);
      spaceship.addChild(windowframe);
      spaceship.addChild(window);
      spaceship.addChild(fin1);
      spaceship.addChild(fin2);
      spaceship.addChild(fin3);
    }
    else if (player==2){
      //drawn from tip of spaceship (origin is at tip)
      spaceship = createShape(GROUP);
      fill(46,82,206);
      PShape tip = createShape(TRIANGLE, 0,0,-25, 30, 25, 30);
      fill(210,216,219);
      PShape body = createShape(RECT, -25,30,50,80);
      fill(133,153, 162);
      PShape windowframe = createShape(ELLIPSE, 0, 50, 35, 35);
      fill(136,211, 245);
      PShape window = createShape(ELLIPSE, 0, 50, 25, 25);
      fill(133,153, 162);
      PShape windowframe2 = createShape(ELLIPSE, 0, 90, 35, 35);
      fill(136,211, 245);
      PShape window2 = createShape(ELLIPSE, 0, 90, 25, 25);
      fill(46,82,206);
      PShape fin1 = createShape(QUAD, -25, 85, -25, 110, -35, 150, -40, 90);
      PShape fin2 = createShape(QUAD, 25, 85, 25, 110, 35, 150, 40, 90);
      spaceship.addChild(tip);
      spaceship.addChild(body);
      spaceship.addChild(windowframe);
      spaceship.addChild(window);
      spaceship.addChild(windowframe2);
      spaceship.addChild(window2);
      spaceship.addChild(fin1);
      spaceship.addChild(fin2);
    }
    
  }
  
  void up(){
    y -=1.1;
  }
  
  void down(){
    y+=1.1;
  }
  
  void left(){
    x-=1.1;
  }
  
  void right(){
    x+=1.1;
  }
  
  void display(Boolean collision){
    pushMatrix();
    //going up and haven't reached the top 
    if (goUp && y > 0){
      up();
    }
    //going down and haven't reached the bottom
    if (goDown && y < height){
     down();
    }
    
    if (goLeft &&  x>0){
      left();
    }
    
    if (goRight && x < width){
      right();
    }
    //reached the top or collision
    if (y <=0 | collision){
      x = initial_x;
      y = initial_y;
    }
    
    //goes beneath screen view
    if (y > height - 50){
      x = initial_x;
      y = initial_y;
    }
    
    
    translate(x,y);
    scale(0.5);
    shape(spaceship, 0, 0 );
    popMatrix();
  }
  
  //determine score
  int Score(Boolean collision){
    //add 5 if reached top
    if (this.y <=1){
      score += 5;
      x = initial_x;
      y = initial_y;
      
    }
    //-1 if collided
    if (collision){
      x = initial_x;
      y = initial_y;
      score -= 1;
    }
    return score;
  }
 
}
