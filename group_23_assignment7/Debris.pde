//starts the debris at random positions
class Debris {
  float x;
  float y;
  float r;
  float angle;
  float rotate;
  float speed;
  boolean left;
  
  Debris(float x, float y, float r, boolean l, float s){
    this.x = x;
    this.y = y;
    this.r = r;
    angle = 0;
    rotate = 0;
    speed = random(s) + 0.5;
    left = l;
  }
  
  //create the central circle and the circles surrounding it
  void display(){
    ellipse(this.x, this.y, this.r, this.r);
    for(int i = 0; i < 5; i++){
      pushMatrix();
      translate(this.x,this.y);
      rotate(rotate);
      popMatrix();
      ellipse(30*cos(angle) + this.x, 30*sin(angle)+ this.y, this.r/2, this.r/2);
      angle += 5;
    }
    rotate+=0.1;
  }
  
  //moves the debris
  void move(boolean direction){
    display();
    if(direction == true){
      this.x -= speed;
    } else{
      this.x += speed;
    }
  }
  
  //makes sure it comes back
  void offscreen(){
    if(this.x >= width+150 || this.x <= -150){
      this.left = !this.left;
    }
  }
}
