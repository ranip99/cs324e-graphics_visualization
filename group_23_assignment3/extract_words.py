#Python
draperies_book = open("draperies.txt", "r")
content =  draperies_book.readline()
words_list = []
output = open("allwords.txt", "w")
output2 = open("uniquewords.txt", "w")
output3 = open("wordfrequency.txt", "w")
while content !="": 
    content = draperies_book.readline()
    words = content.split()
    for i in words: 
        if i.isalpha(): 
            words_list.append(i.lower())
            output.write(i.lower())
            output.write("\n")
        else: 
            word_string=""
            for ch in i: 
                if ch.isalpha():
                    word_string+=ch
            if word_string!="": 
                words_list.append(word_string.lower())
                output.write(word_string.lower())
                output.write("\n")
unique = set(words_list)
for word in unique:
    output2.write(word)
    output2.write("\n")

my_Dict={}
for words in unique: 
    freq = words_list.count(words)
    if freq in my_Dict: 
        my_Dict[freq]+=1
    else: 
        my_Dict[freq]=1

my_Dict = dict(sorted(my_Dict.items()))
for k,v in my_Dict.items():
    output3.write(str(k)+":"+str(v)+"\n")

draperies_book.close()
output.close()
output2.close()
output3.close()

        
        