1. Open the python file and run it.
	If the .txt files are not present already:
		Move uniquewords.txt into the a3_novelvisualization folder
		Move wordfrequency.txt into the a3_word frequency folder

2. For the a3_novelvisualization.pde, download the necessary font and run the file

3. The word frequency graph can be seen in a3_wordfrequency
	Open the .pde file inside it and click run

4. The wordcloud file is present as a png.

