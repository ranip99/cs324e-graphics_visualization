int x;
int y;
String line;
String [] dim;
int index = 0;
int len;

void setup() {
  size(700, 700);
}

void draw() {
  
  String[] freq  = loadStrings("wordfrequency.txt");
  len = freq.length;
  line = freq[index];
  
  dim = split(line, ":");
  x = int(dim[0]);
  y = int(dim[1]);
  
  fill(200, 180, 100);
  rectMode(CENTER);
  rect(width/2, height - x*10, y, 10);
  fill(0);
  
  textSize(40);
  text("Word Frequency", 20, 70);
  text("Relationship", 50, 120);
  
  if (index >= len -1) {
    noLoop();
  }
  index += 1;
}
