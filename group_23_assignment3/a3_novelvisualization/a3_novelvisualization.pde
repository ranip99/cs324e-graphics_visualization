//Processing
PFont papyrus;
int x;
int y;
int size;
int c = 0;
String printed [];
StringList used_words;

void setup() {
    background(70);
    size(700, 600);
    size = 38;
    papyrus = createFont("Papyrus", 30);
    y = 38;
    c = 0;
    
}

void draw() {
    textSize(30);
    textFont(papyrus);
    String[] words = loadStrings("uniquewords.txt");

    used_words = new StringList();
    int index = int(random(words.length));
    String word_Str= words[index];
    
    while (used_words.hasValue(word_Str)) {
      index = int(random(words.length));
      word_Str= words[index];
    }
    
    used_words.append(word_Str);
      
    
    //alternate with 3 different colors
    if (c == 0) {
        fill(230, 156, 116);
        c = 1;
    } else if (c == 1) {
        fill(116, 179, 255);
        c = 2;
    } else {
        fill(193, 99, 245);
        c = 0;
    }

    if (x + (int)textWidth(word_Str) > width) {
        x = 0;
        y += size;
    }

    if (y <= height) {
        text(word_Str +" ", x, y);
        x += 8 + (int)textWidth(word_Str);
    }
}


void mousePressed() {

    background(70);
    x = 0;
    y = 24;
    used_words.clear();
}
